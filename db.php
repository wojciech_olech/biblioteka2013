<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=sql.s10.vdl.pl;dbname=nuxpt_bibl',
			'username'   => 'nuxpt_bibl',
			'password'   => 'bibl2013',
		),
	),
);
