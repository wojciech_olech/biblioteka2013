
<table class="table table-hover" id="mybook-table">
<thead>
	<tr>
		<th>Tytuł</th>
		<th>Autor</th>
		<th>ISBN</th>
		<th>Wypożyczono/rezerwowano</th>		
	</tr>
</thead>
<tbody>
		<?php foreach($books as $book): ?>
			<tr>
				
				<td><?php echo $book['title'] ?></td>
				<td><?php echo $book['author'] ?></td>
				<td><?php echo $book['isbn'] ?></td>
				<td><?php echo Date::forge($book['rent_to'])->format("%d-%m-%Y"); ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>