
<table class="table table-hover" id="book-table">
	<thead>
		<tr>
			<th>Zarządzaj</th>
			<th>Tytuł</th>
			<th>Autor</th>
			<th>ISBN</th>
			<th>Kategoria</th>
			
		</tr>
	</thead>
	<tbody>
		<?php foreach($books as $book): ?>
			<tr>
				<td>
					<button book="<?php echo $book['id']; ?>" class="btn btn-primary btn-sm book-more" data-toggle="modal" data-target="#myModal">Więcej</button>
					<button book="<?php echo $book['id']; ?>" class="btn btn-primary btn-sm book-rent" data-toggle="modal" data-target="#myModal">Rezerwuj</button>
				</td>
				<td><?php echo $book['title'] ?></td>
				<td><?php echo $book['author'] ?></td>
				<td><?php echo $book['isbn'] ?></td>
				<td><?php echo $book['category'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		 $('#book-table').dataTable({
			 "iDisplayLength": 10,
			 "oLanguage": {
			  "sSearch": "Szukaj książki",
			  "sInfo": "Znaleziono _TOTAL_ książel. Aktualnie wyświetlono _START_ do _END_",
		      "oPaginate": {
		        "sNext": "Następna",
			    "sPrevious": "Poprzednia"
		      }
		    }
		 });

		 $('.book-more').live( "click", function() {
			 var bookId = $(this).attr('book');
			 $.ajax({url: "<?php echo Uri::create('book/more');?>"+"?id="+bookId, 
			   type: "GET",
			   success: function(html){
				    $("#modalLabel").html("Więcej informacji:");
					$("#modal-book-body").html(html);
			 	}
			  });
		 });

		 $('.book-rent').live( "click", function() {
			 var bookId = $(this).attr('book');
			 $.ajax({url: "<?php echo Uri::create('book/rent');?>"+"?id="+bookId, 
			   type: "GET",
			   success: function(html){
				   	$("#modalLabel").html("Rezerwacja książki:");
					$("#modal-book-body").html(html);
			 	}
			  });
		 });
	});
	
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalLabel">&nbsp;</h4>
      </div>
      <div class="modal-body" id="modal-book-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->