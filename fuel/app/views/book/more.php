<table class="table table-hover">
	<tr>
		<td><label>Tytuł</label></td>
		<td><?php if(isset($title)) echo $title; ?></td>
	</tr>
	<tr>
		<td><label>Autor</label></td>
		<td><?php if(isset($author)) echo $author; ?></td>
	</tr>
	<tr>
		<td><label>ISBN</label></td>
		<td><?php if(isset($isbn)) echo $isbn; ?></td>
	</tr>
	<tr>
		<td><label>Wydawnictwo</label></td>
		<td><?php if(isset($publisher)) echo $publisher; ?></td>
	</tr>
	<tr>
		<td><label>Rok wydania</label></td>
		<td><?php if(isset($published)) echo $published; ?></td>
	</tr>
	<tr>
		<td><label>Kategoria</label></td>
		<td><?php if(isset($category)) echo $category; ?></td>
	</tr>
</table>
