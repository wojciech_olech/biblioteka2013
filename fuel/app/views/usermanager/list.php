<table class="table table-hover" id="user-table">
	<thead>
		<tr>
			<th>Zarządzaj</th>
			<th>Nazwisko</th>
			<th>Imię</th>
			<th>Email</th>
			<th>Utworzono</th>			
		</tr>
	</thead>
	<tbody>
		<?php foreach($users as $user): ?>
			<tr>
				<td>
					<button user="<?php echo $user['id']; ?>" class="btn btn-primary btn-xs user-more" data-toggle="modal" data-target="#myModal">Wiecej</button>
					<a class="btn btn-danger btn-xs" href="<?php echo Uri::create('usermanager/delete?id='.$user['id']);?>" onclick="return confirm('Czy napewno chcesz usunąć?');">Usuń</a>
				</td>
				<td><?php echo $user['lastname'] ?></td>
				<td><?php echo $user['name'] ?></td>
				<td><?php echo $user['username'] ?></td>
				<td><?php echo Date::forge($user['created_at'])->format("%d-%m-%Y %H:%M") ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function() {
		 

		 $('.user-more').live( "click", function() {
			 $.ajax({url: "<?php echo Uri::create('usermanager/more');?>"+"?id="+$(this).attr('user'), 
			   type: "GET",
			   success: function(html){
				    $("#modalLabel").html("Wiecej informacji:");
					$("#modal-user-body").html(html);
			 	}
			  });
		 });

		 
	});
	
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modalLabel">&nbsp;</h4>
      </div>
      <div class="modal-body" id="modal-user-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->