<table class="table table-hover">
	<tr>
		<td><label>Nazwisko</label></td>
		<td><?php if(isset($lastname)) echo $lastname; ?></td>
	</tr>
	<tr>
		<td><label>Imię</label></td>
		<td><?php if(isset($name)) echo $name; ?></td>
	</tr>
	<tr>
		<td><label>Email</label></td>
		<td><?php if(isset($username)) echo $username; ?></td>
	</tr>
	<tr>
		<td><label>Data utworzenia</label></td>
		<td><?php echo Date::forge($created_at)->format("%d-%m-%Y %H:%M") ?></td>
	</tr>
	<tr>
		<td><label>Ostatnie logowanie</label></td>
		<td><?php if(isset($last_login)) echo Date::forge($last_login)->format("%d-%m-%Y %H:%M"); ?></td>
	</tr>
</table>

<label>Wypożyczone/rezerwowane książki</label>
<div>
	<table class="table table-hover user-book-table">
		<?php foreach($books as $book): ?>
			<tr>
				<td><?php echo $book['title']?></td>
				<td width="50px"><?php echo Date::forge($book['rent_to'])->format("%d-%m-%Y");?></td>
			</tr>				
		<?php endforeach; ?>
	</table>	
</div>
