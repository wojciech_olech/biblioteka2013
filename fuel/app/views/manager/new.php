<div class="panel panel-default">
	<div class="panel-heading">Nowa książka</div>
	<div class="panel-body">
		<form class="form-horizontal" role="form" action="<?php echo Uri::current();?>" method="post">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-2 control-label">Tytuł</label>
				<div class="col-sm-10">
					<input name="title" value="<?php if(isset($title)) echo $title; ?>" class="form-control" id="inputEmail3" placeholder="Tytuł" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Autor</label>
				<div class="col-sm-10">
					<input name="author" value="<?php if(isset($author)) echo $author; ?>" class="form-control" id="inputEmail3" placeholder="Autor" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">ISBN</label>
				<div class="col-sm-10">
					<input name="isbn" value="<?php if(isset($isbn)) echo $isbn; ?>" class="form-control" id="inputEmail3" placeholder="ISBN" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Wydawnictwo</label>
				<div class="col-sm-10">
					<input name="publisher" value="<?php if(isset($publisher)) echo $publisher; ?>" class="form-control" id="inputEmail3" placeholder="Wydawnictwo" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Rok wydania</label>
				<div class="col-sm-10">
					<input name="published" value="<?php if(isset($published)) echo $published; ?>" class="form-control" id="inputEmail3" placeholder="Rok wydania" required>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-2 control-label">Kategoria</label>
				<div class="col-sm-10">
					<input name="category" value="<?php if(isset($category)) echo $category; ?>" class="form-control" id="inputEmail3" placeholder="Kategoria" required>
				</div>
			</div>
			<input name="id" type="hidden" value="<?php if(isset($id)) echo $id; ?>">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Zapisz</button>
				</div>
			</div>
		</form>
	</div>
</div>