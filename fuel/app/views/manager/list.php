
<table class="table table-hover" id="book-table">
	<thead>
		<tr>
			<th>Zarządzaj</th>
			<th>Tytu≥</th>
			<th>Autor</th>
			<th>ISBN</th>
			<th>Kategoria</th>
			
		</tr>
	</thead>
	<tbody>

		<?php foreach($books as $book): ?>
			<tr>
				<td>
					<a class="btn btn-primary btn-xs" href="<?php echo Uri::create('manager/edit?id='.$book['id']);?>">Edytuj</a>
					<a class="btn btn-danger btn-xs" href="<?php echo Uri::create('manager/delete?id='.$book['id']);?>" onclick="return confirm('Czy napewno chcesz usunąć?');">Usuń</a>
				</td>
				<td><?php echo $book['title'] ?></td>
				<td><?php echo $book['author'] ?></td>
				<td><?php echo $book['isbn'] ?></td>
				<td><?php echo $book['category'] ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		 $('#book-table').dataTable({
			 "iDisplayLength": 10,
			 "oLanguage": {
			  "sSearch": "Szukaj książki",
			  "sInfo": "Znaleziono _TOTAL_ książek. Aktualnie wyświetlono _START_ do _END_",
		      "oPaginate": {
		        "sNext": "Następna",
			    "sPrevious": "Poprzednia"
		      }
		    }
		 });
	});
	
</script>
