<form class="form-signin" role="form" action="<?php echo Uri::current();?>" method="post">
	<h2 class="form-signin-heading"><?php echo Lang::get('users.loginlabel'); ?></h2>
	<input type="text" name="username" class="form-control" placeholder="<?php echo Lang::get('users.email'); ?>"
		required autofocus value="<?php echo (isset($errors) ? $username : '') ?>"> 
	<input type="password" name="password" class="form-control"
		placeholder="Hasło" required>
	<button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo Lang::get('users.signin'); ?></button>
</form>
