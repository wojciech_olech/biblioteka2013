<form class="form-signin" role="form" action="<?php echo Uri::current();?>" method="post">
	<h2 class="form-signin-heading">Rejestracja</h2>
	<input type="text" name="name" class="form-control" placeholder="Imię"
		required autofocus> 
	<input type="text" name="lastname" class="form-control" placeholder="Nazwisko"
		required> 
	<input type="text" name="username" class="form-control" placeholder="Adres email"
		required> 
	<input type="password" class="form-control" name="password"
		placeholder="Hasło" required>
	<input type="password" class="form-control" name="repeatpassword"
		placeholder="Powtórz hasło" required> 
	<button class="btn btn-lg btn-primary btn-block" type="submit">Rejestruj</button>
</form>
