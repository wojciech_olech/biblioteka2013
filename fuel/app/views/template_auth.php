<!DOCTYPE html>
<html lang="pl-PL">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=iso-8859-2">
		<title>Biblioteka 2013</title>	
		<?php echo Asset::css('bootstrap.css'); ?>
		<?php echo Asset::css('nuxmanager.css'); ?>
		<?php echo Asset::js('jquery.js'); ?>
		<?php echo Asset::js('bootstrap.min.js'); ?>
		<?php echo Asset::js('jquery.dataTables.js'); ?>
		<?php echo Asset::js('dataTables.bootstrap.js'); ?>
	</head>
	<body>
		<!-- Header -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">					
					<a class="navbar-brand" href="<?php echo Uri::create('welcome');?>">Biblioteka 2013</a>
				</div>		
				<div class="navbar-right" >
					
					<a class="navbar-brand" href="<?php echo Uri::create('users/registration');?>">Rejestracja</a>
				</div>		
			</div>
		</div>		
		
		<div class="jumbotron">
			
		</div>

		<div class="container">
			<?php if(isset($success)) : ?>
				<div class="alert alert-success"><?php echo $success; ?></div>
			<?php endif; ?>
			<?php if(isset($errors)) : ?>
				<div class="alert alert-danger"><?php echo $errors; ?></div>
			<?php endif; ?>			
			
			<div id="content">
				<?php echo $content; ?>
			</div>
			<footer>
				<p>&copy; Biblioteka 2013</p>
			</footer>
		</div> 	
	</body>
</html>