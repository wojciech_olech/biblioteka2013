<?php

class Controller_Book extends Controller_Template {
	
	public function before() {
		parent::before();
		if(!Auth::check()) {
			Response::redirect('users/login');
		}
	}
	
	public function action_list() {		
		if(Input::get('status') == 'ok') {
			$this->template->success = 'Akcja zostala wykonana prawidlowo';
		}
		
		$result = DB::query('SELECT * FROM `books`')->execute();
		$data['books']  = $result->as_array();
		
		$this->template->content = View::forge('book/list', $data);
	}
	
	public function action_more() {
		$result = DB::select()->from('books')->where('id', '=', Input::get('id'))->execute();
		$data = $result->as_array();
		return Response::forge(View::forge('book/more', $data[0]));
	}
	
	public function action_rent() {
		$book = DB::select()->from('books')->where('id', '=', Input::get('id'))->execute()->as_array();
		$bookRent = DB::select()->from('book_rent')->where('book_id', '=', Input::get('id'))->execute()->as_array();
		if (!empty($bookRent)) {
			$currentTime = Date::time()->get_timestamp();
			
			for($i = 0; $i < count($bookRent); $i++) {
				if ($bookRent[$i]['rent_to'] > $currentTime) {
					$data['title'] = $book[0]['title'];
					$data['rentTo'] = $bookRent[$i]['rent_to'];
					return Response::forge(View::forge('book/rent_na', $data));
				}
			}
			
			if ($bookRe > $currentTime) {
				return Response::forge(View::forge('book/rent_na', $book[0]));
			}
		}	
			
		if (Input::post()) {
			list($insert_id, $rows_affected) = DB::insert('book_rent')->set(array(
					'user_id' => Auth::get('id'),
					'book_id' => Input::post('book'),
					'rent_from' => Date::time()->get_timestamp(),
					'rent_to' => Date::time()->get_timestamp()+(60*60*24*7),
			))->execute();
			
			
			Response::redirect('book/list?status=ok');
		}
		
		
		return Response::forge(View::forge('book/rent', $book[0]));
	}
	
	public function action_mybooks() {
		$userId = Auth::get('id');
		
		$books = DB::query("SELECT books.*, book_rent.* FROM books join book_rent on book_rent.book_id=books.id where book_rent.user_id=".$userId." order by book_rent.rent_from ASC")->execute()->as_array();
		
		$data = array();
		$data['books'] = $books;
		$this->template->content = View::forge('book/mybooks', $data);
	}
}
