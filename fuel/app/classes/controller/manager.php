<?php

class Controller_Manager extends Controller_Template {
	
	public function before() {
		parent::before();
		if(Auth::get_profile_fields('role')!='admin') {
			Response::redirect('users/login');
		}
	}
	
	public function action_list() {		
		if(Input::get('status') == 'ok') {
			$this->template->success = 'Akcja zostala wykonana prawidlowo';
		}
		
		$result = DB::query('SELECT * FROM `books`')->execute();
		$data['books']  = $result->as_array();
		
		$this->template->content = View::forge('manager/list', $data);
	}
	
	public function action_new() {
		if (Input::post()) {
			DB::insert('books')->set(array(
			    'title' => Input::post('title'),
			    'author' => Input::post('author'),
			    'isbn' => Input::post('isbn'),
				'publisher' => Input::post('publisher'),
				'published' => Input::post('published'),
				'category' => Input::post('category'),
			))->execute();
			Response::redirect('book/list?status=ok');
		}
		$this->template->content = View::forge('manager/new');
	}
	
	public function action_delete() {
		if(Input::get('id')) {
			DB::delete('books')->where('id', '=', Input::get('id'))->execute();
		}
		Response::redirect('manager/list?status=ok');
	}
	
	public function action_edit() {
		if (Input::post()) {
			DB::update('books')->set(array(
			    'title' => Input::post('title'),
			    'author' => Input::post('author'),
			    'isbn' => Input::post('isbn'),
				'publisher' => Input::post('publisher'),
				'published' => Input::post('published'),
				'category' => Input::post('category'),
			))
			 ->where('id', '=', Input::post('id'))
			->execute();
			Response::redirect('manager/list?status=ok');
		}
		
		if(Input::get('id')) {
			$result = DB::select()->from('books')->where('id', '=', Input::get('id'))->execute();
			$data = $result->as_array();
			$this->template->content = View::forge('manager/new', $data[0]);
		}	
	}
}
