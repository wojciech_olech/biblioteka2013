<?php

class Controller_Project extends Controller_Template {
	
	public function action_list() {
		$this->template->title = Lang::get('users.loginlabel');
		$this->template->content = View::forge('project/list');
	}
	
	public function action_new() {
		$this->template->success = 'yeeee';
		$this->template->title = Lang::get('users.loginlabel');
		$this->template->content = View::forge('project/new');
	}
	
}