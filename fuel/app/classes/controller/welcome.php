<?php

class Controller_Welcome extends Controller_Template {
	
	public $template = 'template_auth';

	public function action_index() {
		$this->template->content = View::forge('welcome/index');
	}

	public function action_404() {
		return Response::forge(ViewModel::forge('welcome/404'), 404);
	}
}
