<?php

class Controller_Usermanager extends Controller_Template {
	
	public function before() {
		parent::before();
		if(Auth::get_profile_fields('role')!='admin') {
			Response::redirect('users/login');
		}
	}
	
	public function action_list() {		
		$users = DB::query('SELECT * FROM `users`')->execute()->as_array();
		
		for($i = 0; $i < count($users); $i++) {
			$users[$i]['profile_fields'] = @unserialize($users[$i]['profile_fields']) ?: array();
			$users[$i]['lastname'] = array_key_exists('lastname', $users[$i]['profile_fields']) ? $users[$i]['profile_fields']['lastname'] : '';
			$users[$i]['name'] = array_key_exists('name', $users[$i]['profile_fields']) ? $users[$i]['profile_fields']['name'] : '';
		}
		$data['users']  = $users;
		$this->template->content = View::forge('usermanager/list', $data);
	}
	
	public function action_more() {
		$userId = Input::get('id');
		$result = DB::select()->from('users')->where('id', '=', $userId)->execute();
		$books = DB::query("SELECT books.*, book_rent.* FROM books join book_rent on book_rent.book_id=books.id where book_rent.user_id=".$userId." order by book_rent.rent_from ASC")->execute()->as_array();
		
		$data = $result->as_array();
		$user = $data[0];
		
		
		$user['profile_fields'] = @unserialize($user['profile_fields']) ?: array();
		$user['lastname'] = array_key_exists('lastname', $user['profile_fields']) ? $user['profile_fields']['lastname'] : '';
		$user['name'] = array_key_exists('name', $user['profile_fields']) ? $user['profile_fields']['name'] : '';
		$user['books'] = $books;
		
		return Response::forge(View::forge('usermanager/more', $user));
	}
	
	public function action_delete() {
		if(Input::get('id')) {
			DB::delete('users')->where('id', '=', Input::get('id'))->execute();
		}
		Response::redirect('usermanager/list?status=ok');
	}
	
}
