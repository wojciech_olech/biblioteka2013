<?php 

class Controller_Users extends Controller_Template {
	
	public $template = 'template_auth';
	
	public function action_login() {
		$data = array();
		$data['username']    = '';
		if (Input::post()) {
			if (Auth::login()) {
				if(Auth::get_profile_fields('role')=='admin') {
					Response::redirect('manager/list');
				} else {
					Response::redirect('book/list');
				}
			} else {
				$data['username']    = Input::post('username');
            	$this->template->errors = Lang::get('users.loginerror');
			}
		}
		$this->template->title = Lang::get('users.loginlabel');
		$this->template->content = View::forge('users/login', $data);
	}
	
	public function action_registration() {
		$data = array();
		if (Input::post()) {
			Auth::create_user(
				Input::post('username'),
				Input::post('password'),
				Input::post('username'),
				1,
				array(
					'name' => Input::post('name'),
					'lastname' => Input::post('lastname'),
					'role' => 'normal',
				)
			);
			$this->template->success = Lang::get('users.registrationsuccess');
			Response::redirect('users/login');
		}
		$this->template->title = Lang::get('users.loginlabel');
		$this->template->content = View::forge('users/registration', $data);
	}
	
	public function action_logout() {
		Auth::logout();
		Response::redirect('users/login');
	}
	
}