<?php
return array(
    'loginlabel' => 'Please sign in',
    'email' => 'Email address',
    'password' => 'Password',
	'signin' => 'Sign in',
);